namespace exercicio_5{
    let numeros: number[] = [1, 2, 3, 4, 5, 6];

    let impares = numeros.filter(function(num) {
    return num % 2 !== 0;
    });
    
    console.log(impares); 
}