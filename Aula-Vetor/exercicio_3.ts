namespace exercicio_3{
    let obj: any = {
        nome: "Cadu",
        idade: 16,
        email: "carlosrubim10@gmail.com"
    
    }
    console.log(obj);
    let livros: any[] = [
        {Titulo: "Título 1", Autor: "Autor1"},
        {Titulo: "Título 2", Autor: "Autor2"},
        {Titulo: "Título 3", Autor: "Autor3"},
        {Titulo: "Título 4", Autor: "Autor4"},
        {Titulo: "Título 5", Autor: "Autor3"},
    ];
    /*let titulos = livros.map((livro) => {
        return livro.Titulo
    });
    console.log(titulos); */
    //console.log(Autor);

    /*Dado um array de objetos livros, contendo os campos títuo e autor, crie um programa em typescript que utilize a função filter () 
    para encontrar todos os livros do autor com valor "Autor 3". Em seguida, utilize a função map() para mostrara apenas os títulos dos livros encontrados.
    O RESULTADO DEVE SER EXIBIDO NO CONSOLE
    */
    
    let autor3 = livros.filter((livro) => {
        return livro.autor === "Autor 3"
        console.log(autor3)
    })
    console.log(autor3);

    let titulos = autor3.map ((autor3) => {
        return autor3.titulo
    })
    console.log(titulos);
    
}
