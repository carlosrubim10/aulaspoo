//Crie uma função que receba um array de números como parâmetro e retorne a soma desses números.
namespace exercicio3{ 
    function soma(numeros:Array<number>) : number{
        let somador = 0
        for(let numero of numeros){
            somador += numero;
        }
        return somador;
    }

    console.log(soma([1,5]));
    
    
}
