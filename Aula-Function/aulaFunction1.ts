//Crie uma função que receba uma string como parâmetro e retorne o número de caracteres da string.
namespace aulaFunction1 {
    function saudacao(nome?:string) {
        if (nome) {
            console.log(`Olá, ${nome}`);
            
        } else {
            console.log("Olá, estranho!");
            
        }
        
    }

    saudacao("Carlos");

    function potencia(base:number, expoente:number = 2) {
        console.log((Math.pow(base,expoente)));
    }
    potencia(2);
    potencia(2,3);
}