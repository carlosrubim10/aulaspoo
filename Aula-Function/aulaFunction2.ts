namespace aulaFunction2 {
    function multiplicarPor(n:number):
    (x:number) => number {
        return function (x:number):number {
            return x * n;
        }
    } 
    let duplicar = multiplicarPor(2);
    let triplicar = multiplicarPor(3);
    let resultado = duplicar(5);
    let resultadoT = triplicar(5);

    console.log(resultado);
    console.log(resultadoT);
    
    
}